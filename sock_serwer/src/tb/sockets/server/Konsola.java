package tb.sockets.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Konsola {

	public static void main(String[] args) {
		Random rand = new Random();
		int wylosowanaLiczba = rand.nextInt(101), podanaLiczba = 0, licznikPodejsc = 1;
		for(int i = 0; i < 15; i++) {
		try {
			ServerSocket sSock = new ServerSocket(9909);
			Socket sock = sSock.accept();
			DataInputStream in = new DataInputStream(sock.getInputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(in));
			podanaLiczba = is.read();
			sprawdzenie(podanaLiczba, wylosowanaLiczba,licznikPodejsc);
			licznikPodejsc++;
			is.close();
			in.close();
			sock.close();
			sSock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(i == 14) przegrana(wylosowanaLiczba);
		if(podanaLiczba == wylosowanaLiczba) break;
		}
	}
	
	static void sprawdzenie(int podana, int wylosowana, int licznikPodejsc) {
		if(podana == wylosowana) {
			System.out.println("Gratulacje! " + podana + " jest prawidlowa odpowiedzia!");
			System.out.println("Liczba Twoich podejsc to: " + licznikPodejsc);
		} else if(podana > wylosowana) System.out.println("Mniej");
		else if(podana < wylosowana) System.out.println("Wiecej");	
	}
	static void przegrana(int wylosowana) {
		System.out.println("Niestety, ale przekroczy�e� limit ruch�w (15) i przegrywasz.");
		System.out.println("Liczb�, ktor� mia�e� zgadn�� by�o " + wylosowana);
	}
	
}
