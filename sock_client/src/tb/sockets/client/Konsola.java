package tb.sockets.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Konsola {

	public static void main(String[] args) {
		
		try {
			int podanaLiczba = 0;
			Scanner skaner = new Scanner(System.in);
			
			System.out.println("Witaj w grze polegaj�cej na zgadywaniu liczby. Twoim zadaniem jest wprowadzanie liczby z przedzia�u <0;100>,");
			System.out.println("kt�ra b�dzie nast�pnie analizowana przez program. Otrzymasz wiadomo�� zwrotn� o tre�ci: wiecej/mniej/trafiles");
			System.out.println("Twoim zadaniem jest jak najszybsze odgadni�cie liczby. Maksymalna ilo�� pr�b to 15, a wi�c");
			System.out.println("zastan�w sie dobrze, zanim udzielisz odpowiedzi. Przyda Ci sie tak�e jaka� dobra taktyka ;-)");
			System.out.println("Powodzenia!");
			System.out.println();
			System.out.println("Podaj liczbe: ");
			
			podanaLiczba = skaner.nextInt();
			Socket sock = new Socket("127.0.0.1", 9909);
			DataOutputStream so = new DataOutputStream(sock.getOutputStream());
			so.write(podanaLiczba);
			so.close();
			sock.close();
			
			} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}


